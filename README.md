# Assistente Pessoal - Apolo

## Equipe

### Dev Team
Geysa Santos  
Matheus Marciano Leite  
Mike Barcelos  
Nicolas Rodrigues  
Ronaldo Galvão  

### Masters
Mayara Ferreira  
Willian Dener  

## Quem é o Apolo?
Um assistente pessoal virtual para plataforma web, com funcionalidades voltadas a facilitação do dia a dia do usuário, voltado ao contexto estudantil.

## E por quê ele foi criado?
Com a atualidade demandando tempo e eficácia em todas as atividades, nada melhor do que ter uma mãozinha que te auxilie nas tarefas. Seja na organização, no lembrete de eventos importantes, ou em uma pesquisa rápida sobre um tema.
Apolo te auxilia em tudo isso e muito mais. Com reconhecimento de voz, com um comando você pode acessar todo conteúdo de uma matéria, pode agendar provas para que o programa te avise quando começar a estudar e inclusive pode calcular a quantidade de faltas restante no semestre sem qualquer dor de cabeça.

## Funcionalidades

* Calendário do aluno;
* Calculadora por voz;
* Anotações por voz;
* Salvar anotações por bloco (matéria);
* Pesquisa no Google por voz;
* Definição de alarmes;
* Visualizar histórico de alarmes;
* Saldar o usuário;
* Tocar faixa de áudio;
* Indentificar e alertar se pode-se ausentar.

## Ferramentas
Utilizamos as seguintes ferramentas:

* Linguagem principal: Python 3.8 ;
* IDE's: PyCharm e/ou Visual Studio Code;
* Front End Web: Javascript;
* Conceitos do SCRUM - Norteador do Projeto;
* Principais bibliotecas:  
    **SpeechRecognition:**  
    **PyAudio:**   


## Pré Requisito

* Possuir dispositivos de áudio e voz instalados:  
        _Gravação:_ microfone;  
        _Reprodução:_ alto-falantes;  
* Possuir acesso a internet;
* Possuir acesso a um browser;
